|REQ/RES| METODO HTTP | URL | HEADERS | STATUS | DESCRIPCIÓN
|----------
| REQ |	GET |	http://zeus.inf.ucv.cl/~ifigueroa/doku.php |	Upgrade-Insecure-Requests,User-Agent,Accept,Acept-Encoding,Accept-Language |  n/a	| El cliente hace la petición |
|
| RES |	n/a |	n/a |	Date,Server,X-Powered,Expires,Cache,Controls,Pragma,X-UA-Compatible,Set-Cookie,Vary,Content-Encoding,Contect-Length,Keep-Alive,Connection,Content-Type |	200	 | El servidor acepta al Nuevo cliente guardándolo dentro de sus datos, al no haber caché|
|
| REQ |GET |	http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/default/bootstrap.min.css |	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie |	n/a |	Indica al navegador los tipos de datos de texto solicitados |
|
|RES |	n/a |	n/a |	Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Content-Encoding,Content-Length,Content-Type |	200 | El Servidor acepta el formato pedido por el cliente |
|
|REQ |	GET |	http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/css.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5 |	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie |	n/a	| Indica al navegador los tipos de datos de texto solicitados |
|
|RES |	n/a |	n/a |	Date,Server,X-Powered,Cache-Control,Pragma,ETag,Last-Modified, Vary,Content-Encoding,Content-Length,Content-Type |	200	| El servidor acepta el formato de texto solicitado |
|
|REQ |	GET	|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/font-awesome/css/font-awesome.min.css |	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie |	n/a	| Indica al navegador los tipos de datos de texto solicitados |
|
|RES|	n/a |	n/a |	Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Content-Encoding,Content-Length,Content-Type |	200	| El servidor acepta el formato de texto solicitado|
| 
| REQ| GET | http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f | User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie | n/a | Se le solicita al navegador el ingreso de aplicaciones java de cualquier tipo MIME |
|
|RES|	n/a |	n/a |	Date,Server,X-Powered,Cache-Control,Pragma,ETag,Last-Modified, Vary,Content-Encoding,Content-Length,Content-Type |	200	 | El servidor acepta la solicitud para aplicaciones java|
|
|REQ|	GET |	http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/js.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5 |	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie |	n/a	| Se le solicita al navegador el ingreso de aplicaciones java de cualquier tipo MIME |
|
|RES |	n/a |	n/a	| Date,Server,X-Powered,Cache-Control,Pragma,ETag,Last-Modified, Vary,Content-Encoding,Content-Length,Content-Type |	200	|El servidor acepta la solicitud para aplicaciones java|
|
|REQ	|GET|	http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/js/bootstrap.min.js	|User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie|	n/a	|Se le solicita al navegador el ingreso de aplicaciones java de cualquier tipo MIME|
|
|RES|	n/a	|n/a|	Date,Server,Last-Modified,ETag,Accept-Ranges,Vary, Content-Encoding,Content-Length,Content-Type|	200	|El servidor acepta la solicitud para aplicaciones java|
|
|REQ|	GET	|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/anchorjs/anchor.min.js|	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie|	n/a	|Se le solicita al navegador el ingreso de aplicaciones java de cualquier tipo MIME|
|
|RES	|n/a|	n/a	|Date,Server,Last-Modified,ETag,Accept-Ranges,Vary, Content-Encoding,Content-Length,Content-Type|	200	|El servidor acepta la solicitud para aplicaciones java|
|
|REQ|	GET	|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/logo.png|	User-Agent,Accept,Referer,Accept-Encoding,Accept,Language,Cookie|	n/a	|Se le solicita al navegador el ingreso de una imagen|
|
|RES|	n/a	|n/a|	Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Content-Type|	200	|Se ha aceptado la solicitud de agregar una imagen de tipo PNG|

